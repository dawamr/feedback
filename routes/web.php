<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', function(){
    Auth::logout();
    return redirect('/');
});



Auth::routes();
Route::get('/register', function(){
    return redirect('/login');
})->name('register');


Route::get('/home', 'HomeController@index')->name('home');
Route::resource('umpanbalik', 'UmpanBalikController');

Route::get('/terimakasih', function(){
    return view('terimakasih');
});


Route::group(['prefix'=>'admin','middleware'=>['auth']], function(){
    Route::get('/', function(){
      return view('admin.index');
    });

    Route::resource('/layanan', 'LayananController');
    Route::resource('/layanan/{id}/pertanyaan', 'PertanyaanController');
    Route::resource('/laporan', 'LaporanController');
    Route::resource('/grafik', 'GrafikController');

});


// Route Laporan
Route::get('/admin/download/pdf', 'DownloadController@indexpdf');

Route::get('/admin/laporan/{id}/download/pdf', 'DownloadController@indexpdf_laporan');

Route::get('/admin/download/excel', 'DownloadController@indexexcel');

Route::get('/admin/laporan/{id}/download/excel', 'DownloadController@indexexcel_laporan');

Route::get('/excel', function() {
  return view('admin.download.index_excel_laporan');
});
