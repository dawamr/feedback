<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Excel;
class DownloadController extends Controller
{
    public function indexpdf()
    {
      $pdf = PDF::loadView('admin.download.index_pdf');
      return $pdf->download('laporan.pdf');
    }

    // public function indexpdf_laporan($id)
    // {
    //   $layanan = \App\Layanan::find($id);
    //   $title = $layanan->title;
    //
    //   return view('admin.download.index_pdf_laporan', compact('title', 'id'));
    //   $pdf = PDF::loadView('admin.download.index_pdf_laporan', compact('title', 'id'));
    //   return $pdf->download('laporan.pdf');
    // }

    public function indexpdf_laporan(Request $request, $id)
    {
      $data = [];

      if($request->date) {

          $date = $request->date;
          //dd($date);
          $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();

        for($i = 0; $i < sizeof($pertanyaan); $i++) {

            $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                              ->where('nilai', 1)
                                              ->whereDate('created_at', $date)
                                              ->count();

            $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                    ->where('nilai', 2)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                    ->where('nilai', 3)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                    ->where('nilai', 4)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;

            if($jumlah_suara != 0){
              $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
            }
          }


          $data['pertanyaan'] = $pertanyaan;

          $data['title'] = \App\Layanan::find($id)->title;
          $data['id'] = $id;
          $data['date'] = $date;

          $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
                                ->whereDate('created_at', $date)
                                ->count();

          // return view('admin.download.index_pdf_laporan')->with('data', $data);
          $pdf = PDF::loadView('admin.download.index_pdf_laporan', $data)
                      ->setPaper('a4', 'portait')
                      ->setWarnings(false);
          return $pdf->download('pertanyaan.pdf');
      }

    $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();

    for($i = 0; $i < sizeof($pertanyaan); $i++) {

        $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                          ->where('nilai', 1)
                                          ->count();

        $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                ->where('nilai', 2)
                                ->count();

        $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                ->where('nilai', 3)
                                ->count();

        $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                ->where('nilai', 4)
                                ->count();

        $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;

        if($jumlah_suara != 0){
          $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
        }
      }


      $data['pertanyaan'] = $pertanyaan;

      $data['title'] = \App\Layanan::find($id)->title;
      $data['id'] = $id;
      $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
                            ->count();

      // return view('admin.download.index_pdf_laporan', $data);

      $pdf = PDF::loadView('admin.download.index_pdf_laporan', $data)
                ->setPaper('a4', 'portait')
                ->setWarnings(false);

        return $pdf->download('pertanyaan.pdf');

    }



    public function indexexcel()
    {
      return \Excel::download(new \App\Exports\invoicesExport, 'laporan.xlsx');
    }

    public function indexexcel_laporan (Request $request, $id)
    {
    //   $data = [];
    //
    //   if($request->date) {
    //
    //       $date = $request->date;
    //       //dd($date);
    //       $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();
    //
    //     for($i = 0; $i < sizeof($pertanyaan); $i++) {
    //
    //         $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                                           ->where('nilai', 1)
    //                                           ->whereDate('created_at', $date)
    //                                           ->count();
    //
    //         $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                                 ->where('nilai', 2)
    //                                 ->whereDate('created_at', $date)
    //                                 ->count();
    //
    //         $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                                 ->where('nilai', 3)
    //                                 ->whereDate('created_at', $date)
    //                                 ->count();
    //
    //         $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                                 ->where('nilai', 4)
    //                                 ->whereDate('created_at', $date)
    //                                 ->count();
    //
    //         $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;
    //
    //         if($jumlah_suara != 0){
    //           $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
    //         }
    //       }
    //
    //
    //       $data['pertanyaan'] = $pertanyaan;
    //
    //       $data['title'] = \App\Layanan::find($id)->title;
    //       $data['id'] = $id;
    //       $data['date'] = $date;
    //
    //       $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
    //                             ->whereDate('created_at', $date)
    //                             ->count();
    //
    //       // return view('admin.download.index_ecel_laporan', $data);
    //       return \Excel::download(new \App\Exports\invoicesExport, 'laporan.xlsx', $data);;
    //   }
    //
    // $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();
    //
    // for($i = 0; $i < sizeof($pertanyaan); $i++) {
    //
    //     $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                                       ->where('nilai', 1)
    //                                       ->count();
    //
    //     $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                             ->where('nilai', 2)
    //                             ->count();
    //
    //     $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                             ->where('nilai', 3)
    //                             ->count();
    //
    //     $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
    //                             ->where('nilai', 4)
    //                             ->count();
    //
    //     $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;
    //
    //     if($jumlah_suara != 0){
    //       $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
    //     }
    //   }
    //
    //
    //   $data['pertanyaan'] = $pertanyaan;
    //
    //   $data['title'] = \App\Layanan::find($id)->title;
    //   $data['id'] = $id;
    //   $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
    //                         ->count();

      // return view('admin.download.index_excel_laporan', $data);
        return \Excel::download(new \App\Exports\InvoicesExport_Excel, 'laporan.xlsx')->view($id);

    }
}
