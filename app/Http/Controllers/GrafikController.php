<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GrafikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.grafik.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
      $nilais1 = \App\Nilai::all()
                ->where('layanan_id', '=', $id)
                ->where('nilai', 1);
      $nilais2 = \App\Nilai::all()
                ->where('layanan_id', '=', $id)
                ->where('nilai', 2);
      $nilais3 = \App\Nilai::all()
                ->where('layanan_id', '=', $id)
                ->where('nilai', 3);
      $nilais4 = \App\Nilai::all()
                ->where('layanan_id', '=', $id)
                ->where('nilai', 4);

      if($request->date){
        $nilais1 = \App\Nilai::where('layanan_id', '=', $id)
                  ->whereDate('created_at', $request->date)
                  ->where('nilai', 1)->get();
        $nilais2 = \App\Nilai::where('layanan_id', '=', $id)
                  ->whereDate('created_at', $request->date)
                  ->where('nilai', 2)->get();
        $nilais3 = \App\Nilai::where('layanan_id', '=', $id)
                  ->whereDate('created_at', $request->date)
                  ->where('nilai', 3)->get();
        $nilais4 = \App\Nilai::where('layanan_id', '=', $id)
                  ->whereDate('created_at', $request->date)
                  ->where('nilai', 4)->get();
      }
        return view('admin.grafik.index',compact('id','nilais1','nilais2','nilais3','nilais4'));
        return view('admin.grafik.index',compact('id','nilais1','nilais2','nilais3','nilais4'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
