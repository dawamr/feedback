<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $pertanyaan = \App\Pertanyaan::where('layanan_id', $id)->get();
        return view('admin.layanan.pertanyaan.index',compact('id'))->with('pertanyaans', $pertanyaan);
        // dd($pertanyaan->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('admin.layanan.pertanyaan.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
      $pertanyaan   = $request->pertanyaan;
      $layanan_id   = $id;

      $pertanyaans =  new \App\Pertanyaan;
      $pertanyaans->pertanyaan = $pertanyaan;
      $pertanyaans->layanan_id = $layanan_id;
      $pertanyaans->save();

      return redirect('/admin/layanan/'.$id.'/pertanyaan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $ids)
    {
      $pertanyaan = \App\Pertanyaan::find($ids);
      return view ('admin.layanan.pertanyaan.edit', compact('id'))->with('pertanyaan', $pertanyaan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $ids)
    {
      $pertanyaan   = $request->pertanyaan;
      $layanan_id   = $id;

      $pertanyaans  = \App\Pertanyaan::find($ids);
      $pertanyaans->pertanyaan  = $pertanyaan;
      $pertanyaans->layanan_id  = $layanan_id;
      $pertanyaans->save();

      return redirect('/admin/layanan/'.$id.'/pertanyaan');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $ids)
    {
      $pertanyaan = \App\Pertanyaan::find($ids);
      $pertanyaan->delete();

      return redirect('/admin/layanan/'.$id.'/pertanyaan');
    }
}
