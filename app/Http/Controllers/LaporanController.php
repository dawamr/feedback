<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $date = $request->date;
        if($date){

          $layanans = \App\Layanan::get();

          for($i = 0; $i < sizeof($layanans); $i++) {

            $layanans[$i]->buruk = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                              ->where('nilai', 1)
                                              ->whereDate('created_at', $date)
                                              ->count();

            $layanans[$i]->biasa = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                    ->where('nilai', 2)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $layanans[$i]->baik = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                    ->where('nilai', 3)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $layanans[$i]->sangatbaik = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                    ->where('nilai', 4)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $layanans[$i]->kunjungan = \App\Kunjungan::where('layanan_id', $layanans[$i]->id)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $jumlah_suara = $layanans[$i]->buruk + $layanans[$i]->biasa + $layanans[$i]->baik + $layanans[$i]->sangatbaik;

            if($jumlah_suara != 0){
              $layanans[$i]['rata2'] = (($layanans[$i]->buruk*1) +($layanans[$i]->biasa*2) +($layanans[$i]->baik*3) +($layanans[$i]->sangatbaik*4))/$jumlah_suara;
            }
          }

          $data['layanan'] = $layanans;

          $data['date'] = $date;

          return view('admin.laporan.index')->with('data', $data);
        }

        $layanans = \App\Layanan::get();

        for($i = 0; $i < sizeof($layanans); $i++) {

          $layanans[$i]->buruk = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                            ->where('nilai', 1)
                                            ->count();

          $layanans[$i]->biasa = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                  ->where('nilai', 2)
                                  ->count();

          $layanans[$i]->baik = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                  ->where('nilai', 3)
                                  ->count();

          $layanans[$i]->sangatbaik = \App\Nilai::where('layanan_id', $layanans[$i]->id)
                                  ->where('nilai', 4)
                                  ->count();

          $layanans[$i]->kunjungan = \App\Kunjungan::where('layanan_id', $layanans[$i]->id)
                                  ->count();

          $jumlah_suara = $layanans[$i]->buruk + $layanans[$i]->biasa + $layanans[$i]->baik + $layanans[$i]->sangatbaik;

          if($jumlah_suara != 0){
            $layanans[$i]['rata2'] = (($layanans[$i]->buruk*1) +($layanans[$i]->biasa*2) +($layanans[$i]->baik*3) +($layanans[$i]->sangatbaik*4))/$jumlah_suara;
          }
        }

        $data['layanan'] = $layanans;

        $data['date'] = $date;




        return view('admin.laporan.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
      $data = [];

      if($request->date) {

          $date = $request->date;
          //dd($date);
          $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();

        for($i = 0; $i < sizeof($pertanyaan); $i++) {

            $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                              ->where('nilai', 1)
                                              ->whereDate('created_at', $date)
                                              ->count();

            $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                    ->where('nilai', 2)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                    ->where('nilai', 3)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                    ->where('nilai', 4)
                                    ->whereDate('created_at', $date)
                                    ->count();

            $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;

            if($jumlah_suara != 0){
              $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
            }
          }


          $data['pertanyaan'] = $pertanyaan;

          $data['title'] = \App\Layanan::find($id)->title;
          $data['id'] = $id;
          $data['date'] = $date;

          $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
                                ->whereDate('created_at', $date)
                                ->count();


          return view ('admin.laporan.show')->with('data', $data);
      }

    $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();

    for($i = 0; $i < sizeof($pertanyaan); $i++) {

        $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                          ->where('nilai', 1)
                                          ->count();

        $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                ->where('nilai', 2)
                                ->count();

        $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                ->where('nilai', 3)
                                ->count();

        $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                ->where('nilai', 4)
                                ->count();

        $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;

        if($jumlah_suara != 0){
          $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
        }
      }


      $data['pertanyaan'] = $pertanyaan;

      $data['title'] = \App\Layanan::find($id)->title;
      $data['id'] = $id;
      $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
                            ->count();

      return view ('admin.laporan.show')->with('data', $data);}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
