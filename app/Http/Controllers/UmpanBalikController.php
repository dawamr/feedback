<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UmpanBalikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('umpanbalik.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $daftar_pertanyaan = $request->input('data');
        // dd($daftar_pertanyaan);
        $nilai = 0;
        foreach($daftar_pertanyaan as $pertanyaan){
          $nilai = $nilai + $pertanyaan['nilai'];
        }
        $nilai = $nilai / sizeof($daftar_pertanyaan);
  
        $kunjungan = new \App\Kunjungan;
        $kunjungan->layanan_id = $id;
        $kunjungan->rata2 = $nilai;
        $kunjungan->save();
  
        foreach($daftar_pertanyaan as $pertanyaan){
          $nilai = new \App\Nilai;
          $nilai->layanan_id = $id;
          $nilai->kunjungan_id = $kunjungan->id;
          $nilai->pertanyaan_id = $pertanyaan['id'];
          $nilai->nilai = $pertanyaan['nilai'];
          $nilai->save();
        }
  
  
        return 'ok';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
