<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    public function title(){
      return $this->belongsTo('\App\Layanan','layanan_id','id');
    }

    public function nilai(){
      return $this->hasMany('App\Nilai','layanan_id','id');
    }
}
