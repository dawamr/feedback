<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

class InvoicesExport implements \Maatwebsite\Excel\Concerns\FromView
{

  public function view(): View {

    return view('admin.download.index_excel');
  }
}


?>
