<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

class InvoicesExport_Excel implements \Maatwebsite\Excel\Concerns\FromView
{

  public function view(): View {

    $data = [];

    if($request->date) {

        $date = $request->date;
        //dd($date);
        $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();

      for($i = 0; $i < sizeof($pertanyaan); $i++) {

          $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                            ->where('nilai', 1)
                                            ->whereDate('created_at', $date)
                                            ->count();

          $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                  ->where('nilai', 2)
                                  ->whereDate('created_at', $date)
                                  ->count();

          $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                  ->where('nilai', 3)
                                  ->whereDate('created_at', $date)
                                  ->count();

          $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                  ->where('nilai', 4)
                                  ->whereDate('created_at', $date)
                                  ->count();

          $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;

          if($jumlah_suara != 0){
            $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
          }
        }


        $data['pertanyaan'] = $pertanyaan;

        $data['title'] = \App\Layanan::find($id)->title;
        $data['id'] = $id;
        $data['date'] = $date;

        $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
                              ->whereDate('created_at', $date)
                              ->count();

        // return view('admin.download.index_ecel_laporan', $data);
        return \Excel::download(new \App\Exports\invoicesExport, 'laporan.xlsx', $data);;
    }

  $pertanyaan = \App\Pertanyaan::where('layanan_id','=', $id)->get();

  for($i = 0; $i < sizeof($pertanyaan); $i++) {

      $pertanyaan[$i]->buruk = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                                        ->where('nilai', 1)
                                        ->count();

      $pertanyaan[$i]->biasa = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                              ->where('nilai', 2)
                              ->count();

      $pertanyaan[$i]->baik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                              ->where('nilai', 3)
                              ->count();

      $pertanyaan[$i]->sangatbaik = \App\Nilai::where('pertanyaan_id', $pertanyaan[$i]->id)
                              ->where('nilai', 4)
                              ->count();

      $jumlah_suara = $pertanyaan[$i]->buruk + $pertanyaan[$i]->biasa + $pertanyaan[$i]->baik + $pertanyaan[$i]->sangatbaik;

      if($jumlah_suara != 0){
        $pertanyaan[$i]['rata2'] = (($pertanyaan[$i]->buruk*1) +($pertanyaan[$i]->biasa*2) +($pertanyaan[$i]->baik*3) +($pertanyaan[$i]->sangatbaik*4))/$jumlah_suara;
      }
    }


    $data['pertanyaan'] = $pertanyaan;

    $data['title'] = \App\Layanan::find($id)->title;
    $data['id'] = $id;
    $data['kunjungan'] = \App\Kunjungan::where('layanan_id', $id)
                          ->count();

    return view('admin.download.index_excel_laporan')->with('data', $data);
  }
}

?>
