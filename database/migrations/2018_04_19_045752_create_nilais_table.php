<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kunjungan_id')->unsigned();
            $table->integer('layanan_id')->unsigned();
            $table->integer('pertanyaan_id')->unsigned();
            $table->integer('nilai');
            $table->timestamps();

            $table->foreign('layanan_id')->references('id')
                  ->on('layanans')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('pertanyaan_id')->references('id')
                  ->on('pertanyaans')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('kunjungan_id')->references('id')
                  ->on('kunjungans')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilais');
    }
}
