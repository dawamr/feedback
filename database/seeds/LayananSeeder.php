<?php

use Illuminate\Database\Seeder;

class LayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('layanans')->insert([
        //     'title' => 'HELPDESK',
        //     'avatar' => 'handshake-flat.png',
        // ]);
        // DB::table('layanans')->insert([
        //     'title' => 'LAYANAN',
        //     'avatar' => 'icon.png',
        // ]);
        $layanan = new \App\Layanan;
        $layanan->title = "HELPDESK";
        $layanan->avatar = "handshake-flat.png";
        $layanan->save();

        $layanan = new \App\Layanan;
        $layanan->title = "ADUAN";
        $layanan->avatar = "icon.png";
        $layanan->save();

    }
}
