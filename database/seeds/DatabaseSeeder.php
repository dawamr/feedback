<?php

use Illuminate\Database\Seeder;
use \App\Layanan;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(LayananSeeder::class);
        $this->call(PertanyaanSeeder::class);
    }
}
