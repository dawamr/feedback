<?php

use Illuminate\Database\Seeder;

class PertanyaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pertanyaan = new \App\Pertanyaan;
        $pertanyaan->layanan_id = 1;
        $pertanyaan->pertanyaan = "Bagaimana pendapat anda, tentang keramahan pelayanan petugas kami?";
        $pertanyaan->save();

        $pertanyaan = new \App\Pertanyaan;
        $pertanyaan->layanan_id = 1;
        $pertanyaan->pertanyaan = "Bagaimana pendapat anda, tentang kemampuan petugas kami dalam menerangkan persyaratan dan prosedur perizinan?";
        $pertanyaan->save();

        $pertanyaan = new \App\Pertanyaan;
        $pertanyaan->layanan_id = 1;
        $pertanyaan->pertanyaan = "Bagaimana pendapat anda, tentang kenyamanan tempat pelayanan kami?";
        $pertanyaan->save();

        $pertanyaan = new \App\Pertanyaan;
        $pertanyaan->layanan_id = 1;
        $pertanyaan->pertanyaan = "Bagaimana pendapat anda, tentang ketepatan waktu dan prosedur pelayanan perizinan di kantor kami?";
        $pertanyaan->save();

        $pertanyaan = new \App\Pertanyaan;
        $pertanyaan->layanan_id = 2;
        $pertanyaan->pertanyaan = "Bagaimana menurut anda, pelayanan petugas aduan di kantor kami?";
        $pertanyaan->save();
    }
}
