@extends('layouts.umpanbalik')
@section('content')
    <section class="text-center p-3 text-light">
      <img src="../img/logo.png" height="100" alt="">
      <h5><strong>FEEDBACK SEDERHANA</strong></h5>
      <h6><strong>Aplikasi Umpan Balik Sederhana</strong></h6>
      <h4><strong>by Uangkelas</strong></h4>
    </section>
    <div class="container-fluid text-center text-light">
      <h4 class="h"> PILIH LAYANAN </h4>
      <hr color="white" width="60%"><br>
      <div class="row justify-content-center ">
        <?php
            $layanan = \App\Layanan::all();
        ?>
        @for ($i=0; $i < sizeof($layanan); $i++)

        <div class="col-3">
          <a href="/umpanbalik/{{$layanan[$i]->id}}"> <img src="../uploads/{{$layanan[$i]->avatar}}" class="e" height="200" width="200"  alt=""></a>
          <br><br>
          <h3 class="t">{{$layanan[$i]->title}}</h3>
        </div>

        @endfor

      </div>
    </div>
@endsection
