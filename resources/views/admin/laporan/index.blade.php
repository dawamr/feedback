@extends('layouts.umpanbalik')
@section('content')

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/admin">DPMPTSP KUDUS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">


    </ul>
    <form class="form-inline my-2 my-lg-0">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
    </form>
  </div>
</nav>


<div class="container">
  <div class="container-fluid p-5 mt-5 mb-5" style="background: white">
    <h1 class="text-center" style="font-family: impact; color: #42a5bf; letter-spacing: 3px">
      LAPORAN
    </h1>
    <div class="row">
      <div class="col-md-3">
        <div class="row">
          <a href="/admin" class="btn btn-success btn-md text-right">Kembali</a>
          <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Download
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="/admin/download/pdf">PDF</a>
              <a class="dropdown-item" href="/admin/download/excel">Excel</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-9">
        <form class="row form-group" action="/admin/laporan" method="GET">
          {{csrf_field()}}
          <div class="row">
            <div class="col">
              <input type="date" class="form-control" name="date" value="{{$data['date']}}" placeholder="First name">
            </div>
            <div class="col">
              <button type="submit" class="btn btn-primary">Cari</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <hr>

    <table class="table">
      <tr>
        <th>No</th>
        <th>Layanan</th>
        <th>Jumlah Kunjungan</th>
        <th>Buruk (1)</th>
        <th>Biasa (2)</th>
        <th>Bagus (3)</th>
        <th>Sangat Bagus (4)</th>
        <th>Rata Rata</th>
        <th>Action</th>
      </tr>
      @php
        $no =1;
      @endphp
      @foreach ($data['layanan'] as $layanan)
        @php
          // $kunjungan = \App\Kunjungan::where('layanan_id', $layanan->id)->count();
        @endphp
        <tr>

          <td>{{$no++}}</td>
          <td>{{$layanan->title}}</td>
          <td>{{$layanan->kunjungan}}</td>
          <td>{{$layanan->buruk}}</td>
          <td>{{$layanan->biasa}}</td>
          <td>{{$layanan->baik}}</td>
          <td>{{$layanan->sangatbaik}}</td>

          <td>{{number_format($layanan->rata2,2)}}</td>

          <td>
            <a href="/admin/laporan/{{$layanan->id}}" class="btn btn-primary">Detail</a>
          </td>
        </tr>
      @endforeach
    </table>

  </div>
</div>
<footer class="container-fluid bg-light p-5 text-center" style="color: grey">
    Copyright © 2018 DPMPTSP KUDUS
</footer>
@endsection
