<?php error_reporting(0) ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

    google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
          var data = google.visualization.arrayToDataTable([
          ['Year', 'Buruk',
                   'Biasa',
                   'Bagus',
                   'Sangat Bagus'],
          ['2018', {{count($nilais1)}},
                   {{count($nilais2)}},
                   {{count($nilais3)}},
                   {{count($nilais4)}}],

          ]);

      var options = {
        chart: {
          title: 'Data Penilaian',
          subtitle: 'Buruk, Biasa, Baik, Sangat Baik',
        },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['red', 'yellow', 'green', 'blue']
      };

      var chart = new google.charts.Bar(document.getElementById('chart_div'));

      chart.draw(data, google.charts.Bar.convertOptions(options));

      var btns = document.getElementById('btn-group');

      btns.onclick = function (e) {

      if (e.target.tagName === 'BUTTON') {
        options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
        chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
    }
    </script>

    <style>
      .e{
        border-radius: 50%;
        transition: 0.5s;
      }
      .e:hover{
        box-shadow: 7px 7px 10px #297184;
        transition: 0.5s;
      }
      .btn-outline-primary{
        margin-bottom: 20%;
      }
    </style>

    <title></title>
  </head>
  <body style="background: #77d6ef">


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/admin">FEEDBACK</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
    </form>
  </div>
</nav>
    <div class="container">
      <div class="container-fluid p-5 text-center mt-5 mb-5" style="background: white">
        <h1 style="font-family: impact; color: #42a5bf; letter-spacing: 3px">
          <?php
            $layanan = \App\Layanan::find($id);
          ?>
          GRAFIK LAYANAN {{$layanan->title}}
        </h1>
        <hr>
        <h5 class="text-left" style="font-family: impact;color: #42a5bf; letter-spacing: 2px">
          PILIH LAYANAN
        </h5>
        <?php
          $layanans = \App\Layanan::get();
        ?>
        <div class="form-group">
          <select class="form-control" onchange="location = this.value;">
            <option value="/admin/grafik/0"> PILIH LAYANAN </option>
            @foreach ( $layanans as $layanan)
              <option value="/admin/grafik/{{$layanan->id}}"> {{$layanan->title}} </option>
            @endforeach
          </select>
        </div>
        @if($id > 0)
          <div class="row">
            <div class="col-md-3">
              <a href="/admin/laporan/{{$id}}" class="btn btn-primary">Detail</a>
            </div>
            <div class="col-md-9">
              <form class="row form-group" action="/admin/grafik/{{$id}}" method="GET">
                <div class="row">
                  <div class="col">
                    <input type="date" class="form-control" name="date" placeholder="First name">
                  </div>
                  <div class="col">
                    <button type="submit" class="btn btn-success">Cari</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
           <div id="chart_div"></div>
        @endif
      </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  </body>
</html>
