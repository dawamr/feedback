<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


    <style>
      .e{
        border-radius: 50%;
        transition: 0.5s;
      }
      .e:hover{
        box-shadow: 7px 7px 10px #297184;
        transition: 0.5s;
      }
      .btn-outline-primary{
        margin-bottom: 20%;
      }
    </style>

    <title></title>
  </head>
  <body>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/admin">FEEDBACK</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" >Logout</a>
    </form>
  </div>
</nav>
    <div class="container-fluid text-center p-5 text-light" style="background: #42a5bf">
      <h1 style="font-family: impact;letter-spacing: 3px"> Admin Manager </h1>

      <hr width="60%" color="white"><br>

      <div class="row justify-content-center">
        <div class="col-md-3 col-sm-3 text-center">
          <a href="/admin/grafik"> <img src="https://icon-icons.com/icons2/474/PNG/512/graphs_46879.png" class="e" width="200px"  alt=""></a>
          <br><br>
          <h3 style="font-family: calibri; font-weight: bold">Grafik</h3>
        </div>
        <div class="col-md-3 col-sm-3 text-center">
          <a href="/admin/laporan"> <img src="{{URL::asset('uploads/reports.png')}}" class="e" width="200px"  alt=""></a>
          <br><br>
          <h3 style="font-family: calibri; font-weight: bold">Laporan</h3>
        </div>
        <div class="col-md-3 col-sm-3 text-center">
          <a href="/admin/layanan"> <img src="http://eaduan.puspakom.com.my/images/icon_aduan_am_new.png" class="e" width="200px"  alt=""></a>
          <br><br>
          <h3 style="font-family: calibri; font-weight: bold">Layanan</h3>
        </div>

      </div>
    </div>
    <!-- <div class="container-fluid p-5 text-center" style="background: white">
      <h1 style="font-family: impact; color: #42a5bf; letter-spacing: 3px">GRAFIK VOTING</h1>
      <div id="columnchart_material" style="width: 100%; height: 500px;"></div>
    </div> -->
    <footer class="container-fluid bg-light p-5 text-center" style="color: grey">
       Copyright &copy 2019 Uangkelas
    </footer>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  </body>
</html>
