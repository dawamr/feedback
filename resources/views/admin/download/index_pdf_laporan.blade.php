<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    .table {
      border: 1px solid;
    }
    .p {
      padding: 10px;
    }
    .p-2 {
      padding: 5px;
    }
    .text-center {
      text-align: center; text-center
    }
    </style>
  </head>
  <body>
      <h1 class="text-center">LAPORAN <?php echo $title;  ?> </h1>
      <table border="1" class="table" width="">
       <tr class="p">
         <th class="p">No</th>
         <th class="p">Pertanyaan</th>
         <th class="p">Buruk</th>
         <th class="p">Biasa</th>
         <th class="p">Bagus</th>
         <th class="p">Sangat Bagus</th>
         <th class="p">Rata Rata</th>
       </tr>
       @php
         $no =1;
       @endphp
       @foreach($pertanyaan as $pertanyaans)
         <tr class="p-2">
           <td class="p-2 text-center">{{$no++}}</td>
           <td class="p-2">{{$pertanyaans->pertanyaan}}</td>
           <td class="p-2 text-center">{{$pertanyaans->buruk}}</td>
           <td class="p-2 text-center">{{$pertanyaans->biasa}}</td>
           <td class="p-2 text-center">{{$pertanyaans->baik}}</td>
           <td class="p-2 text-center">{{$pertanyaans->sangatbaik}}</td>
           <td class="p-2 text-center">{{number_format($pertanyaans->rata2,2)}}</td>
         </tr>
       @endforeach
      </table>
  </body>
</html>
