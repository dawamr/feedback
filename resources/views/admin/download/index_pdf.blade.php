<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    .table {
      border: 1px solid;
    }
    .p {
      padding: 10px;
    }
    .p-2 {
      padding: 5px;
    }
    .text-center {
      text-align: center; text-center
    }
    </style>
  </head>
  <body>
      <h1 class="text-center">LAPORAN</h1>
      <table border="1" class="table" width="100%">
       <tr class="p">
         <th class="p">No</th>
         <th class="p">Layanan</th>
         <th class="p">Buruk</th>
         <th class="p">Biasa</th>
         <th class="p">Bagus</th>
         <th class="p">Sangat Bagus</th>
         <th class="p">Rata Rata</th>
       </tr>

      <?php

      $no =1;
      $layanans = \App\Layanan::get();


      foreach ($layanans as $layanan){

          $buruk = \App\Nilai::where('layanan_id', $layanan->id)
                                  ->where('nilai', 1)->count();
          $biasa = \App\Nilai::where('layanan_id', $layanan->id)
                                  ->where('nilai', 2)->count();
          $baik = \App\Nilai::where('layanan_id', $layanan->id)
                                  ->where('nilai', 3)->count();
          $sangatbaik = \App\Nilai::where('layanan_id', $layanan->id)
                                  ->where('nilai', 4)->count();
          $jumlah_suara = $buruk + $biasa + $baik +$sangatbaik;
          $rata2 = ($buruk*1 +$biasa*2 +$baik*3 +$sangatbaik*4)/$jumlah_suara;


         echo '<tr><td class="p-2 text-center">'.$no++.'</td>';
         echo '<td class="p-2">'.$layanan->title.'</td>';
         echo '<td class="p-2 text-center">'.$buruk.'</td>';
         echo '<td class="p-2 text-center">'.$biasa.'</td>';
         echo '<td class="p-2 text-center">'.$baik.'</td>';
         echo '<td class="p-2 text-center">'.$sangatbaik.'</td>';

         echo '<td class="p-2 text-center">'.number_format($rata2,2).'</td></tr>';



  }
      ?>


      </table>
  </body>
</html>
