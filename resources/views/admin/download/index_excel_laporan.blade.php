
      <h1 class="text-center">LAPORAN {{$data['title']}} </h1>
      <table>
        <tr>
          <td>No</td>
          <td>Pertanyaan</td>
          <td>Buruk</td>
          <td>Biasa</td>
          <td>Bagus</td>
          <td>Sangat Bagus</td>
        </tr>
        @php
          $no =1;
        @endphp
        @foreach($data['pertanyaan'] as $pertanyaan)
          <tr>
            <td>{{$no++}}</td>
            <td>{{$pertanyaan->pertanyaan}}</td>
            <td>{{$pertanyaan->buruk}}</td>
            <td>{{$pertanyaan->biasa}}</td>
            <td>{{$pertanyaan->baik}}</td>
            <td>{{$pertanyaan->sangatbaik}}</td>
            <td>{{number_format($pertanyaan->rata2,2)}}</td>
          <tr>
        @endforeach
      </table>
