@extends('layouts.umpanbalik')

@section('content')
  <div class="card container mt-5 p-5">
    <div class="container mt-3 text-primary">
      <form action="/admin/layanan" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <h3 class="text-center" style="font-weight: bold;">Tambah Layanan</h3>
        <hr>
        <fieldset class="form-group">
          <label for="exampleInputEmail1">Title</label>
          <input type="text" name="title" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Judul" required autofocus>
        </fieldset>
        <fieldset class="form-group">
          <label for="exampleInputEmail1">Foto</label>
          <input type="file" name="avatar" class="form-control" id="file" required>
        </fieldset>
        <button type="submit" class="btn btn-md btn-primary">Simpan</button>
        <a href="/admin/layanan" class="btn btn-danger">Kembali</a>
      </form>
    </div>
  </div>
@endsection
