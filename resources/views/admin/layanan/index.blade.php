@extends('layouts.umpanbalik')

@section('content')
  <div class="container">
    <div class="card mt-5 p-5">
      <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title text-center" style="font-family: impact; letter-spacing: 2px; color: #585858;">DATA LAYANAN</h3>
            <a href="/admin/layanan/create" class="btn btn-success btn-md text-right">Tambah Layanan</a>
            <a href="/admin" class="btn btn-md btn-danger">Kembali</a><br><br>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-hover text-center">
              <tr class="text-center">
                <th scope="col float-center">No</th>
                <th scope="col float-center">Title</th>
                <th scope="col float-center">Avatar</th>
                <th scope="col float-center">Action</th>
              </tr>
              <!-- Button trigger modal -->
              @php
                $layanan = \App\Layanan::get();
              @endphp
              @for ($i=0; $i < sizeof($layanan); $i++)
                <tbody>
                  <tr>
                    <th scope="row">{{ $i+1}}</th>
                    <td>{{ $layanan[$i]->title}}</td>
                    <td class="text-center"><img src="../uploads/{{$layanan[$i]->avatar}}" alt="" height="80"> </td>
                    <td class="text-center">
                      <a href="/admin/layanan/{{$layanan[$i]->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                      <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{$layanan[$i]->id}}">
                        Hapus
                      </button>
                      <a href="/admin/layanan/{{$layanan[$i]->id}}/pertanyaan" class="btn btn-sm btn-warning text-light">Lihat</a>
                    </td>
                  </tr>
                </tbody>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal{{$layanan[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ngodingo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin menghapus list <strong class="text-danger">"{{$layanan[$i]->title}}"</strong>?</h5>
                      </div>
                      <div class="modal-footer">
                        <form  action="/admin/layanan/{{$layanan[$i]->id}}" method="POST">
                            {{ method_field('DELETE')}}
                              {{csrf_field()}}
                          <input class="btn btn-danger btn-sm" type="submit" value="YA">
                          <a class="btn btn-info btn-sm text-white" data-dismiss="modal" role="button">Tidak</a>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

              @endfor

            </table>
          </div>
        </div>
        <!-- /.box -->
    </div>
  </div>
@endsection
