@extends('layouts.umpanbalik')

@section('content')
  <div class="card p-5 mt-5 container">

    <div class="container m-2">
      <div class="container mt-4 text-primary">
        <h3 class="text-center"><strong>Edit Layanan</strong></h3>

      <hr>

      <form action="/admin/layanan/{{$layanan->id}}" method="post" enctype="multipart/form-data">
        {{ method_field('PATCH')}}
        {{ csrf_field() }}
        <fieldset class="form-group">
          <label for="exampleInputEmail1">Judul</label>
          <input type="text" name="title" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Judul" value="{{$layanan->title }}" >
        </fieldset>
        <fieldset class="form-group">
          <label for="exampleInputEmail1">Foto</label><br>
          <input type="file" name="avatar" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Judul" value="{{$layanan->avatar}}" required autofocus>
        </fieldset>

        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="/admin/layanan" class="btn btn-danger">Kembali</a>
      </form>
    </div>
  </div>
</div>
@endsection
