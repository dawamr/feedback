@extends('layouts.umpanbalik')

@section('content')
  <div class="card p-5 mt-5 container">

    <div class="container m-2">
      <div class="container mt-4 text-primary">
        <h3 class="text-center"><strong>Edit Pertanyaan</strong></h3>

      <hr>

      <form action="/admin/layanan/{{$id}}/pertanyaan/{{$pertanyaan->id}}" method="post" enctype="multipart/form-data">
        {{ method_field('PATCH')}}
        {{ csrf_field() }}
        <fieldset class="form-group">
          <label for="exampleInputEmail1">Pertanyaan</label>
          <input type="text" name="pertanyaan" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Pertanyaan" value="{{$pertanyaan->pertanyaan }}" >
          <input type="hidden" name="" value="{{$id}}">
        </fieldset>

        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="/admin/layanan/{{$id}}/pertanyaan" class="btn btn-danger">Kembali</a>
      </form>
    </div>
  </div>
</div>
@endsection
