@extends('layouts.umpanbalik')

@section('content')
  <div class="card container mt-5 p-5">
    <div class="container mt-3 text-primary">
      <form action="/admin/layanan/{{$id}}/pertanyaan" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <h3 class="text-center" style="font-weight: bold;">Tambah Pertanyaan</h3>
        <hr>
        <fieldset class="form-group">
          <label for="exampleInputEmail1">Pertanyaan</label>
          <input type="text" name="pertanyaan" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Pertanyaan" required autofocus>
          <input type="hidden" name="layanan_id" value="{{$id}}">
        </fieldset>
        <button type="submit" class="btn btn-md btn-primary">Simpan</button>
        <a href="/admin/layanan/{{$id}}/pertanyaan" class="btn btn-danger">Kembali</a>
      </form>
    </div>
  </div>
@endsection
