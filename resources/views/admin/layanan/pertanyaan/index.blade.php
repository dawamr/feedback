@extends('layouts.umpanbalik')

@section('content')
  <div class="container">
    <div class="card mt-5 p-5">
      <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title text-center" style="font-family: impact; letter-spacing: 2px; color: #585858;">DATA LAYANAN</h3>
            <a href="/admin/layanan/{{$id}}/pertanyaan/create" class="btn btn-success btn-md text-right">Tambah Layanan</a>
            <a href="/admin/layanan/" class="btn btn-md btn-danger">Kembali</a><br><br>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-hover">
              <tr class="text-center">
                <th scope="col float-center">No</th>
                <th scope="col float-center" style="width: 60%;">Pertanyaan</th>
                <th scope="col float-center">Action</th>
              </tr>
              <!-- Button trigger modal -->

                <tbody>
                @for ($i=0; $i < sizeof($pertanyaans); $i++)
                  <tr>
                    <th scope="row">{{ $i+1}}</th>
                    <td>{{ $pertanyaans[$i]->pertanyaan}}</td>
                    <td class="text-center">
                      <a href="/admin/layanan/{{$id}}/pertanyaan/{{$pertanyaans[$i]->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                      <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{$pertanyaans[$i]->id}}">
                        Hapus
                      </button>
                    </td>
                  </tr>
                </tbody>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal{{$pertanyaans[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">DPMPTSP KUDUS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <h5 class="modal-title" id="exampleModalLabel">Apakah anda akan menghapus pertanyaan: <strong class="text-danger">"{{$pertanyaans[$i]->pertanyaan}}"</strong>?</h5>
                      </div>
                      <div class="modal-footer">
                        <form  action="/admin/layanan/{{$id}}/pertanyaan/{{$pertanyaans[$i]->id}}" method="POST">
                            {{ method_field('DELETE')}}
                              {{csrf_field()}}
                          <input class="btn btn-danger btn-sm" type="submit" value="YA">
                          <a class="btn btn-info btn-sm text-white" data-dismiss="modal" role="button">Tidak</a>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

              @endfor
            </table>
          </div>
        </div>
        <!-- /.box -->
    </div>
  </div>
@endsection
