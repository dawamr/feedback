@extends('layouts.umpanbalik')
@section('css')
<style media="screen">
.card:hover{
        opacity: 0.5;
        transition: 0.5s;
      }
      .card{
        transition: 0.5s;
        border-radius: 20px;
      }
      .b{
        border: solid 1px white;
        box-shadow: 0px 0px 50px #a5ccf7;
      }
      .card:focus{
        background: red;
      }
      .bnext{
        border-radius: 2px;
      }
</style>
@endsection
@section('content')
<div class="container b mt-5 p-5">
          <h2 id="pertanyaan" class="pt-4 text-center text-white" style="font-family : impact; letter-spacing:2px">
            Bagaimana Pendapat Anda dengan Layanan Kami ?
          </h2>
          <hr color="white">
          <br>


          <div class="row">

            <div class="col-md-3">
              <div class="card text-center p-5 vote" nilai="1">
                <a href="#" class="nav-link">
                  <img src="{{URL::asset('img/buruk.jpeg')}}" width="100" alt=""><br>
                  <strong class="text-muted">Buruk</strong>
                </a>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card text-center p-5 vote" nilai="2">
                <a href="#" class="nav-link">
                  <img src="{{URL::asset('img/c.jpeg')}}" width="100" alt=""><br>
                  <strong class="text-muted text-muted">Biasa</strong>
                </a>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card text-center p-5 vote"  nilai="3">
                <a href="#" class="nav-link">
                  <img src="{{URL::asset('img/b.jpeg')}}" width="100" alt=""><br>
                  <strong class="text-muted text-muted">Bagus</strong>
                </a>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card text-center p-5 vote"  nilai="4">
                <a href="#" class="nav-link">
                  <img src="{{URL::asset('img/sb.jpeg')}}" width="100" alt=""><br>
                   <strong class="text-muted text-muted">Sangat Bagus</strong>
                </a>
              </div>
            </div>



          </div>
          <br>
          <hr color="white">
          <!-- <div class="text-right"><br>
            <button type="button" name="button" class="btn btn-md btn-primary bnext">SELANJUTNYA</button>
          </div> -->
</div>

@endsection
<?php
  // $layanan = \App\Layanan::find($id);
  $pertanyaan = \App\Pertanyaan::where('layanan_id', $id)->get();
  // dd($pertanyaan);
?>
@section('js')
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">
  var daftar_pertanyaan = {!!$pertanyaan!!};
  var pertanyaan_aktif = 0;
  $('#pertanyaan').html(daftar_pertanyaan[0].pertanyaan);
  $('.vote').click(function(){
    if(pertanyaan_aktif == daftar_pertanyaan.length){
      return;
    }
    var nilai = $(this).attr('nilai');
    daftar_pertanyaan[pertanyaan_aktif].nilai = nilai;
    pertanyaan_aktif++;
    if(pertanyaan_aktif == daftar_pertanyaan.length){
      axios.put('/umpanbalik/{{$id}}', {data: daftar_pertanyaan})
          .then(function(resp){
            // console.log(resp);
            location.href = '/terimakasih';
            // alert('sukses');
          })
          .catch(function(err){
            console.log(err);
          });
      return;
    }
    $('#pertanyaan').html(daftar_pertanyaan[pertanyaan_aktif].pertanyaan);

  });
</script>
@endsection
