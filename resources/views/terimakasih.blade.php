@extends('layouts.umpanbalik')
@section('css')
<style media="screen">
.card:hover{
        opacity: 0.5;
        transition: 0.5s;
      }
      .card{
        transition: 0.5s;
        border-radius: 20px;
      }
      .b{
        border: solid 1px white;
        box-shadow: 0px 0px 50px #a5ccf7;
      }
      .card:focus{
        background: red;
      }
      .bnext{
        border-radius: 2px;
      }
</style>
@endsection
@section('content')
<div class="container b mt-5 p-5">
         <h1 align="center" style="color:white;">Terimakasih</h1>
         <h6 align="center" style="color:white;">halaman ini akan kembali otomatis</h6>
</div>

@endsection

@section('js')
<script>
  setTimeout(() => {
    window.location.href = "/";
  }, 1000);
</script>

@endsection


